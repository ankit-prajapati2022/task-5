import React from "react";
import { AppContext } from "../Context";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

const Register = () => {
  const { signUp, isLoading, error, user } = React.useContext(AppContext);

  const navigate = useNavigate();

  const [regData, setRegData] = React.useState({
    name: "",
    email: "",
    password: "",
    role: "regular",
  });

  React.useEffect(() => {
    if (!isLoading) {
      const i = toast.info("Note Email and password are case sensitive", {
        toastId: "custom-id-yes",
      });
      if (user && user.name) {
        toast.update(i, {
          render: "Logged in as " + user.name,
          type: "success",
        });
        setTimeout(() => {
          navigate("/");
        }, 2000);
      }
      if (error) {
        toast.update(i, {
          render: error,
          type: "error",
        });
      }
    }
  }, [isLoading]);

  const handleChange = (e) => {
    setRegData({ ...regData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("password : ", regData.password);
    signUp(regData)
  };

  return (
    <div className="row d-flex justify-content-center my-4">
      <h1 className="display-1 text-center">Register</h1>
      <form className="col-lg-6 col-md-8 my-4 py-3" onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="exampleInputEmail1" className="form-label">
            Email address
          </label>
          <input
            type="email"
            className="form-control"
            id="exampleInputEmail1"
            name="email"
            onChange={handleChange}
            value={regData.email}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="name" className="form-label">
            Name
          </label>
          <input
            type="text"
            className="form-control"
            id="name"
            name="name"
            onChange={handleChange}
            value={regData.name}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="exampleInputPassword1" className="form-label">
            Password
          </label>
          <input
            type="password"
            className="form-control"
            id="exampleInputPassword1"
            name="password"
            onChange={handleChange}
            value={regData.password}
          />
        </div>
        <div className="form-check">
          <input
            className="form-check-input"
            type="radio"
            name="role"
            id="flexRadioDefault1"
            value="admin"
            onChange={handleChange}
          />
          <label className="form-check-label" htmlFor="flexRadioDefault1">
            Admin
          </label>
        </div>
        <div className="form-check">
          <input
            className="form-check-input"
            type="radio"
            name="role"
            id="flexRadioDefault2"
            value="regular"
            onChange={handleChange}
            defaultChecked
          />
          <label className="form-check-label" htmlFor="flexRadioDefault2">
            Regular
          </label>
        </div>
        <button type="submit" className="btn btn-primary my-3">
          Submit
        </button>
      </form>
    </div>
  );
};

export default Register;
