import React from "react";
import { Routes, Route } from "react-router-dom";
import Register from "./Register/Register";
import Login from "./Login/Login";
import Home from "./Home/Home";
import Details from "./Details/Details";
import RestaurantForm from "./RestaurantForm/RestaurantForm";
import NavBar from "./NavBar/NavBar";
import Dashboard from "./Dashboard/Dashboard";
import { ToastContainer } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";

const Main = () => {
  return (
    <div>
      <NavBar />
      <ToastContainer
        position="top-center"
        autoClose={2000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
      />
      <div className="container my-2">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route
            path="/restaurants/create"
            element={<RestaurantForm type="Create" />}
          />
          <Route path="/restaurants/:id" element={<Details />} />
          <Route
            path="/restaurants/:id/edit"
            element={<RestaurantForm type="Edit" />}
          />
          <Route path="*" element={<h1>404</h1>} />
        </Routes>
      </div>
    </div>
  );
};

export default Main;
