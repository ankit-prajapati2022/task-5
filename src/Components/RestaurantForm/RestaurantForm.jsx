import React from "react";
import { useParams, useNavigate } from "react-router-dom";
import { AppContext } from "../Context";
import { toast } from "react-toastify";

const RestaurantForm = (props) => {
  const navigate = useNavigate();
  const { user } = React.useContext(AppContext);
  if (!user || user.role !== "admin") navigate("/");

  const [restaurant, setRestaurant] = React.useState({
    name: "",
    location: "",
    avgRating: 0,
  });
  const { id } = useParams();

  React.useEffect(() => {
    if (props.type === "Edit") {
      fetch(`http://localhost:8000/restaurants/${id}`)
        .then((res) => res.json())
        .then((data) => {
          setRestaurant({ ...data });
          toast.success("Restaurant loaded", { toastId: "custom-id-yes" });
        })
        .catch((err) => {
          console.log(err);
          toast.error("Error loading restaurant");
        });
    }
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      restaurant.name === "" ||
      restaurant.location === "" ||
      restaurant.avgRating === 0
    ) {
      toast.error("Please fill in all fields");
      return;
    }
    const data = {
      ...restaurant,
    };
    if (props.type === "Create") {
      fetch("http://localhost:8000/restaurants", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        body: JSON.stringify(data),
      })
        .then((res) => res.json())
        .then((res) => {
          if (res.id) {
            toast.success("Restaurant created");
            navigate(`/restaurants/${res.id}`);
          } else {
            toast.error(res);
          }
        })
        .catch((err) => {
          console.log(err);
          toast.error("Error creating restaurant");
        });
    } else {
      fetch(`http://localhost:8000/restaurants/${id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        body: JSON.stringify(data),
      })
        .then((res) => res.json())
        .then((res) => {
          if (res.id) {
            toast.success("Restaurant updated", { toastId: "custom-id-yes" });
            navigate(`/restaurants/${res.id}`);
          } else {
            toast.error(res);
          }
        })
        .catch((err) => {
          console.log(err);
          toast.error("Error updating restaurant");
        });
    }
  };

  const handleDelete = () => {
    fetch(`http://localhost:8000/restaurants/${id}`, {
      method: "DELETE",
    })
      .then((res) => res.json())
      .then((res) => {
        toast.success("Restaurant deleted");
        navigate("/");
      })
      .catch((err) => {
        console.log(err);
        toast.error("Error deleting restaurant");
      });
  };

  return (
    <div>
      <div className="form-body">
        <div className="row">
          <div className="form-holder">
            <div className="form-content">
              <div className="form-items">
                <h3>{`${props.type} Restaurant`}</h3>
                <p>Fill in the data below.</p>
                <form className="requires-validation">
                  <div className="col-md-12 mb-3">
                    <label>Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="name"
                      placeholder="Enter name"
                      value={restaurant.name}
                      onChange={(e) =>
                        setRestaurant({ ...restaurant, name: e.target.value })
                      }
                      required
                    />
                  </div>
                  <div className="col-md-12 mb-3">
                    <label>Location</label>
                    <input
                      type="text"
                      className="form-control"
                      id="location"
                      placeholder="Enter location"
                      value={restaurant.location}
                      onChange={(e) =>
                        setRestaurant({
                          ...restaurant,
                          location: e.target.value,
                        })
                      }
                      required
                    />
                  </div>
                  <div className="col-md-12 mb-3">
                    <label>Rating</label>
                    <input
                      type="number"
                      className="form-control"
                      id="rating"
                      placeholder="Enter rating"
                      value={restaurant.avgRating}
                      onChange={(e) =>
                        setRestaurant({
                          ...restaurant,
                          avgRating:
                            e.target.value <= 5 && e.target.value >= 0
                              ? e.target.value
                              : 0,
                        })
                      }
                      required
                    />
                  </div>
                  <div className="form-button mt-3">
                    <button
                      onClick={handleSubmit}
                      id="submit"
                      type="submit"
                      className="btn btn-primary mb-3"
                    >
                      {props.type}
                    </button>
                    
                  </div>
                </form>
                {props.type === "Edit" && (
                      <button
                        onClick={handleDelete}
                        className="btn btn-danger"
                      >
                        Delete
                      </button>
                    )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RestaurantForm;
