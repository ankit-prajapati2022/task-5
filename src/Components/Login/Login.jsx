import React from "react";
import { AppContext } from "../Context";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

const Login = () => {
  const { login, isLoading, error, user } = React.useContext(AppContext);
  const [loginData, setLoginData] = React.useState({
    email: "",
    password: "",
  });

  const navigate = useNavigate();

  React.useEffect(() => {
    if (!isLoading) {
      const i = toast.info("Note Email and password are case sensitive", { toastId: "custom-id-yes" });
      if (user && user.name) {
        toast.update(i, {
          render: "Logged in as " + user.name,
          type: "success",
        });
        setTimeout(() => {
          navigate("/");
        }, 2000);
      }
      if (error) {
        toast.update(i, {
          render: error,
          type: "error",
        });
      }
    }
  }, [isLoading]);

  const handleChange = (e) => {
    setLoginData({ ...loginData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    login(loginData);
  };

  return (
    <>
      <div className="row d-flex justify-content-center my-4">
        <h1 className="display-1 text-center">Login</h1>
        <form className="col-lg-6 col-md-8 my-4 py-3" onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label">
              Email address
            </label>
            <input
              type="email"
              className="form-control"
              id="exampleInputEmail1"
              name="email"
              onChange={handleChange}
              value={loginData.email}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="exampleInputPassword1" className="form-label">
              Password
            </label>
            <input
              type="password"
              className="form-control"
              id="exampleInputPassword1"
              name="password"
              onChange={handleChange}
              value={loginData.password}
            />
          </div>
          <button type="submit" className="btn btn-primary my-3">
            Submit
          </button>
        </form>
      </div>
    </>
  );
};

export default Login;
