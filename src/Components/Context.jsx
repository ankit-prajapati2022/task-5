import React from "react";

const AppContext = React.createContext({});

const Context = ({ children }) => {
  const userData = localStorage.getItem("user") || null;
  const tokenData = localStorage.getItem("token") || "";
  const [user, setUser] = React.useState(JSON.parse(userData));
  const [userJwt, setUserJwt] = React.useState(tokenData);
  const [isLoading, setIsLoading] = React.useState(false);
  const [error, setError] = React.useState(null);

  React.useEffect(() => {
    if (user) {
      localStorage.setItem("user", JSON.stringify(user));
      localStorage.setItem("token", userJwt);
    }
  }, [user]);

  const signUp = (userData) => {
    setIsLoading(true);
    fetch("http://localhost:8000/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(userData),
    })
      .then((res) => res.json())
      .then((res) => {
        setIsLoading(false);
        if (res.user) {
          setUser(res.user);
          setUserJwt(res.accessToken);
        } else {
          setError(res);
        }
      })
      .catch((err) => {
        setIsLoading(false);
        setError(err);
      });
  };

  const login = (data) => {
    setIsLoading(true);
    setError(null);
    fetch("http://localhost:8000/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((res) => {
        setIsLoading(false);
        if (res.user) {
          setUser(res.user);
          setUserJwt(res.accessToken);
        } else {
          setError(res);
        }
      })
      .catch((err) => {
        setIsLoading(false);
        setError(err);
      });
  };

  const logout = () => {
    setUser(null);
    setUserJwt("");
    localStorage.removeItem("user");
    localStorage.removeItem("token");
  };

  return (
    <AppContext.Provider
      value={{ login, signUp, logout, user, userJwt, isLoading, error }}
    >
      {children}
    </AppContext.Provider>
  );
};

export default Context;
export { AppContext };
