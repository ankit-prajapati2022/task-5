import React from "react";
import { AppContext } from "../Context";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

const Dashboard = () => {
  const { user, logout } = React.useContext(AppContext);
  const navigate = useNavigate();
  if (!user || user.role !== "admin") navigate("/");

  //list all users
  const [users, setUsers] = React.useState([]);
  const [editUser, setEditUser] = React.useState({
    id: "",
    name: "",
    email: "",
    role: "",
    password: "",
  });

  React.useEffect(() => {
    fetch("http://localhost:8000/users")
      .then((res) => res.json())
      .then((data) => {
        setUsers(data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const handleDelete = () => {
    fetch(`http://localhost:8000/users/${editUser.id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        setUsers(users.filter((user) => user.id !== editUser.id));
        document.getElementById("modalclose").click();
        toast.success("User deleted successfully");
        if (editUser.id == user.id) {
          logout();
          navigate("/");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleeditUser = () => {
    if (editUser.name === "" || editUser.email === "")
      toast.error("Please fill all fields");

    const data = {
      name: editUser.name,
      email: editUser.email,
      role: editUser.role,
    };

    fetch(`http://localhost:8000/users/${editUser.id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((res) => {
        setUsers(users.map((user) => (user.id === editUser.id ? res : user)));
        document.getElementById("modalclose").click();
        toast.success("User updated successfully");
        if (editUser.id == user.id) {
          logout();
          navigate("/");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const modaluser = (id) => {
    return (
      <>
        <button
          type="button"
          className="btn btn-sm bg-primary rounded-pill"
          data-bs-toggle="modal"
          data-bs-target="#exampleMo"
          onClick={() => setEditUser(id)}
        >
          <i className="bi bi-pencil-square"></i> &nbsp;Edit
        </button>
        <div
          className="modal fade"
          id="exampleMo"
          tabIndex="-1"
          aria-labelledby="exampleModalLa"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLa">
                  Edit User
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body mt-2">
                <div className="contact-form row my-2 px-5">
                  <div className="form-field col-lg-6">
                    <input
                      id="rating"
                      className="input-text js-input"
                      type="text"
                      value={editUser.name}
                      onChange={(e) => {
                        setEditUser({ ...editUser, name: e.target.value });
                      }}
                      required
                    />
                    <label className="label" htmlFor="rating">
                      Name
                    </label>
                  </div>
                  <div className="form-field col-lg-6 ">
                    <input
                      id="comment"
                      className="input-text js-input"
                      type="text"
                      value={editUser.email}
                      onChange={(e) =>
                        setEditUser({ ...editUser, email: e.target.value })
                      }
                      required
                    />
                    <label className="label" htmlFor="comment">
                      Email
                    </label>
                  </div>
                  <div className="form-field col-lg-6">
                    <select
                      className="form-select js-input input-text mt-4"
                      value={editUser.role}
                      onChange={(e) => {
                        setEditUser({ ...editUser, role: e.target.value });
                      }}
                    >
                      <option value="admin">Admin</option>
                      <option value="regular">Regular</option>
                    </select>
                  </div>
                </div>
              </div>

              <div className="modal-footer">
                <button
                  id="modalclose"
                  type="button"
                  className="btn btn-secondary"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
                <button
                  onClick={handleeditUser}
                  type="button"
                  className="btn btn-warning"
                >
                  Save changes
                </button>
                <button
                  onClick={handleDelete}
                  type="button"
                  className="btn btn-danger"
                >
                  Delete
                </button>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };

  return (
    <div>
      {users.map((user) => (
        <div className="list-group m-2" key={user.id}>
          <div className="list-group-item d-flex justify-content-between align-items-center">
            <div className="ms-2 me-auto">
              <div className="fw-bold">{user.name}</div>
              <div className="fw">{user.role}</div>
              <div className="fw-light">{user.email}</div>
            </div>
            {modaluser(user)}
          </div>
        </div>
      ))}
    </div>
  );
};

export default Dashboard;
