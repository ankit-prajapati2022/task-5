import React from "react";
import { Link } from "react-router-dom";
import ReactPaginate from "react-pagination-library";
import "react-pagination-library/build/css/index.css";

const Home = () => {
  const [list, setList] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  const [pageNumer, setPageNumer] = React.useState(1);
  const [filter, setFilter] = React.useState({
    value: "",
    type: "name",
  });
  const [filterlist, setFilterList] = React.useState([]);

  React.useEffect(() => {
    fetch("http://localhost:8000/restaurants")
      .then((res) => res.json())
      .then((data) => {
        setList(data);
        setLoading(false);
        setFilterList(data);
      });
  }, []);

  React.useEffect(() => {
    if (filter.type === "name") {
      setFilterList(
        list.filter((item) =>
          item.name.toLowerCase().includes(filter.value.toLowerCase())
        )
      );
    }
    if (filter.type === "location") {
      setFilterList(
        list.filter((item) =>
          item.location.toLowerCase().includes(filter.value.toLowerCase())
        )
      );
    }
    if (filter.type === "rating") {
      setFilterList(list.filter((item) => item.avgRating >= filter.value));
    }
    setPageNumer(1);
  }, [filter, list]);

  if (loading) {
    return <div>Loading...</div>;
  }

  const elePerPage = 9;
  const pageVisited = (pageNumer - 1) * elePerPage;

  const listcreate = filterlist
    .slice(pageVisited, pageVisited + elePerPage)
    .map((item) => (
      <div className="list-group m-2" key={item.id}>
        <Link
          to={`/restaurants/${item.id}`}
          type="button"
          className="list-group-item list-group-item-action d-flex justify-content-between align-items-start"
        >
          <div className="ms-2 me-auto">
            <div className="fw-bold">{item.name}</div>
            <div className="fw-light">{item.location}</div>
          </div>
          <span className="badge bg-primary rounded-pill">
            <i className="bi bi-star-fill"></i> &nbsp;{item.avgRating}
          </span>
        </Link>
      </div>
    ));

  const searchbutton = () => (
    <div className="d-flex justify-content-center m-4">
      <div className="input-group mb-3">
        <input
          type={filter.type === "rating" ? "number" : "text"}
          value={filter.value}
          min={filter.type === "rating" ? "0" : ""}
          max={filter.type === "rating" ? "5" : ""}
          onChange={(e) => setFilter({ ...filter, value: e.target.value })}
          className="form-control"
          aria-label="Text input with dropdown button"
        />
        <button
          className="btn btn-outline-secondary dropdown-toggle"
          type="button"
          data-bs-toggle="dropdown"
          aria-expanded="false"
        >
          {filter.type.toUpperCase()}
        </button>
        <ul className="dropdown-menu dropdown-menu-end">
          <li onClick={() => setFilter({ ...filter, type: "name" })}>Name</li>
          <li onClick={() => setFilter({ ...filter, type: "location" })}>
            Location
          </li>
          <li onClick={() => setFilter({ ...filter, type: "rating" })}>
            Rating
          </li>
        </ul>
        <button type="button" className="btn btn-success w-25">
          Search
        </button>
      </div>
    </div>
  );

  return (
    <div>
      {searchbutton()}
      {listcreate}
      <div className="d-flex justify-content-center">
        <ReactPaginate
          totalPages={Math.ceil(filterlist.length / elePerPage)}
          changeCurrentPage={(page) => setPageNumer(page)}
          currentPage={pageNumer}
        />
      </div>
    </div>
  );
};

export default Home;
