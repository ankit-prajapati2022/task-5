import React from "react";
import { useParams } from "react-router-dom";
import { AppContext } from "../Context";
import { toast } from "react-toastify";

const Review = () => {
  const { id } = useParams();
  const { user } = React.useContext(AppContext);

  const [review, setReview] = React.useState({ rating: 0, comment: "" });
  const [editReview, setEditReview] = React.useState({
    rating: 0,
    comment: "",
    id: 0,
  });
  const [reviewList, setReviewList] = React.useState([]);
  const [displayReview, setDisplayReview] = React.useState({
    Lastest: undefined,
    Highest: undefined,
    Lowest: undefined,
  });

  React.useEffect(() => {
    fetch(`http://localhost:8000/restaurants/${id}/comments`)
      .then((res) => res.json())
      .then((data) => setReviewList(data));
  }, []);

  React.useEffect(() => {
    if (reviewList.length > 0) {
      reviewList.sort((a, b) =>
        a.date < b.date ? -1 : a.date > b.date ? 1 : 0
      );
      const Lastest = reviewList[reviewList.length - 1];
      reviewList.sort((a, b) => a.avgRating - b.avgRating);
      const Highest = reviewList[reviewList.length - 1];
      const Lowest = reviewList[0];
      setDisplayReview({
        Lastest,
        Highest,
        Lowest,
      });
    }
  }, [reviewList]);

  const handleeditReview = () => {
    if (editReview.rating === 0 || editReview.comment === "") {
      toast.error("Please fill all fields");
      return;
    }
    const data = {
      avgRating: editReview.rating,
      comment: editReview.comment,
      date: new Date(),
    };

    fetch(`http://localhost:8000/comments/${editReview.id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.error) {
          toast.error(res.error);
        } else {
          const newlist = reviewList.filter((item) => item.id !== res.id);
          setReviewList([...newlist, res]);
          document.getElementById("modalclose").click();
          setEditReview({ rating: 0, comment: "", id: 0 });
          toast.success("Review updated successfully");
        }
      })
      .catch((err) => {
        toast.error(err);
      });
  };

  const handleDelete = () => {
    fetch(`http://localhost:8000/comments/${editReview.id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.error) {
          toast.error(res.error);
        } else {
          const newlist = reviewList.filter((item) => item.id !== editReview.id);
          setReviewList([...newlist]);
          document.getElementById("modalclose").click();
          setEditReview({ rating: 0, comment: "", id: 0 });
          toast.success("Review deleted successfully");
        }
      })
      .catch((err) => {
        toast.error(err);
      });
  };
  const modal = (id) => {
    return (
      <div className="card-footer">
        <button
          type="button"
          className="btn btn-primary"
          data-bs-toggle="modal"
          data-bs-target="#exampleModal"
          onClick={() => {
            setEditReview({
              rating: displayReview[id].avgRating,
              comment: displayReview[id].comment,
              id: displayReview[id].id,
            });
          }}
        >
          Edit <i className="bi bi-pencil mx-3"></i>
        </button>
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Edit Comment
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body mt-2">
                <div className="contact-form row my-2 px-5">
                  <div className="form-field col-lg-6">
                    <input
                      id="rating"
                      className="input-text js-input"
                      type="number"
                      value={editReview.rating}
                      onChange={(e) =>
                        setEditReview({
                          ...editReview,
                          rating:
                            e.target.value <= 5 && e.target.value >= 0
                              ? e.target.value
                              : 0,
                        })
                      }
                      required
                    />
                    <label className="label" htmlFor="rating">
                      Rating
                    </label>
                  </div>
                  <div className="form-field col-lg-6 ">
                    <input
                      id="comment"
                      className="input-text js-input"
                      type="text"
                      value={editReview.comment}
                      onChange={(e) =>
                        setEditReview({
                          ...editReview,
                          comment: e.target.value,
                        })
                      }
                      required
                    />
                    <label className="label" htmlFor="comment">
                      Comment
                    </label>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  id="modalclose"
                  type="button"
                  className="btn btn-secondary"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
                <button
                  onClick={handleeditReview}
                  type="button"
                  className="btn btn-warning"
                >
                  Save changes
                </button>
                <button
                  onClick={handleDelete}
                  type="button"
                  className="btn btn-danger"
                >
                  Delete
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const handleSubmit = () => {
    if (review.rating === 0 || review.comment === "") {
      toast.error("Please fill all fields");
      return;
    }

    const data = {
      avgRating: review.rating,
      comment: review.comment,
      restaurantId: id,
      userId: user.id,
      date: new Date(),
    };

    setReview({ rating: 0, comment: "" });

    fetch("http://localhost:8000/comments", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.error) {
          toast.error(data.error);
        } else {
          toast.success("Review added successfully");
          setReviewList([...reviewList, data]);
        }
      });
  };

  return (
    <div>
      {user && user.role !== "admin"  ? (
        <div className="contact-form row my-5 px-5">
          <div className="form-field col-lg-6">
            <input
              id="rating"
              className="input-text js-input"
              type="number"
              value={review.rating}
              onChange={(e) =>
                setReview({
                  ...review,
                  rating:
                    e.target.value <= 5 && e.target.value >= 0
                      ? e.target.value
                      : 0,
                })
              }
              required
            />
            <label className="label" htmlFor="rating">
              Rating
            </label>
          </div>
          <div className="form-field col-lg-6 ">
            <input
              id="comment"
              className="input-text js-input"
              type="text"
              value={review.comment}
              onChange={(e) =>
                setReview({ ...review, comment: e.target.value })
              }
              required
            />
            <label className="label" htmlFor="comment">
              Comment
            </label>
          </div>
          <div className="form-field col-lg-12 mt-3">
            <button className="submit-btn" onClick={handleSubmit}>
              <span>Submit</span>
              <i className="bi bi-arrow-right-circle-fill mx-3"></i>
            </button>
          </div>
        </div>
      ) : (
        <h2 className="text-center">"Login as regular user to write a review"</h2>
      )}

      {Object.keys(displayReview).map(
        (item, i) =>
          displayReview[item] && (
            <div key={i} className="card my-2">
              <div className="card-header d-flex justify-content-between align-items-start">
                <h5 className="card-title">{item} Rating</h5>
                <span className="badge bg-primary rounded-pill">
                  <i className="bi bi-star-fill"></i> &nbsp;
                  {displayReview[item].avgRating}
                </span>
              </div>
              <div className="card-body">
                <blockquote className="blockquote mb-0">
                  <p>{displayReview[item].comment}</p>
                  <footer className="blockquote-footer fs-6">
                    {new Date(displayReview[item].date).toDateString()}
                  </footer>
                </blockquote>
              </div>
              {user &&
                (user.id === displayReview[item].userId ||
                  user.role === "admin") &&
                modal(item)}
            </div>
          )
      )}
    </div>
  );
};

export default Review;
