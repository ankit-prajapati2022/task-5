import React from "react";
import Review from "./Review";
import { AppContext } from "../Context";
import { useParams, useNavigate } from "react-router-dom";

const Details = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const { user } = React.useContext(AppContext);
  const [restaurant, setRestaurant] = React.useState({});

  React.useEffect(() => {
    fetch(`http://localhost:8000/restaurants/${id}`)
      .then((res) => res.json())
      .then((data) => {
        setRestaurant(data);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div>
      <div className="card border-dark my-3">
        <div className="card-header d-flex justify-content-between align-items-start">
          Restaurant Details
          <span className="badge bg-primary rounded-pill">
            <i className="bi bi-star-fill"></i> &nbsp; {restaurant.avgRating}
          </span>
        </div>
        <div className="card-body text-dark">
          <h1 className="card-title ">{restaurant.name}</h1>
          <h6 className="card-subtitle mb-2 text-muted">{restaurant.location}</h6>
          <p className="card-text">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Hic
            exercitationem veniam veritatis id quia, amet aliquam libero cumque
            quibusdam ullam deleniti pariatur iure alias dolorem odit eaque quos
            adipisci neque nesciunt nam qui. Sequi quas, tempora debitis
            nesciunt quaerat ab!
          </p>
        </div>
        {user && user.role === "admin" && (
          <div className="card-footer">
            <div className="d-flex justify-content-between align-items-center">
              <button
                onClick={() => navigate(`/restaurants/${id}/edit`)}
                type="button"
                className="btn btn-outline-primary"
              >
                Edit Restaurant
              </button>
            </div>
          </div>
        )}
      </div>
      <Review />
    </div>
  );
};

export default Details;
